from pycoingecko import CoinGeckoAPI
import pandas as pd
from datetime import datetime
import mysql.connector
import time
import asyncio
from dataclasses import dataclass
import requests
import json
import numpy as np
from multiprocessing.dummy import Pool


cg = CoinGeckoAPI()

@dataclass
class CryptoData:
    id: int
    price: float
    marketcap: float
    totalVolume: float
    date: datetime
    currencyId: str

def syncInsert(symbol, name):
    tic = time.perf_counter()
    bitcoinHistory = cg.get_coin_market_chart_by_id(name, 'usd', 0, 'daily')
    
    df = pd.DataFrame(bitcoinHistory)
    prices = pd.DataFrame(df.prices.tolist(), columns=['timestamp', 'price'])
    market_caps = pd.DataFrame(df.market_caps.tolist(), columns=['timestamp', 'market_cap'])
    total_volumes = pd.DataFrame(df.total_volumes.tolist(), columns=['timestamp', 'total_volume'])

    prices.price = prices.price.interpolate()
    market_caps.market_cap = market_caps.market_cap.interpolate()
    total_volumes.total_volume = total_volumes.total_volume.interpolate()

    prices.index = pd.to_datetime(prices.timestamp, unit="ms")
    market_caps.index = pd.to_datetime(market_caps.timestamp, unit="ms")
    total_volumes.index = pd.to_datetime(total_volumes.timestamp, unit="ms")
    prices = prices.drop('timestamp', axis=1)
    
    prices['marketcap'] = market_caps['market_cap']
    prices['total_volume'] = total_volumes['total_volume']
    toc = time.perf_counter()
    print('Fetch', toc-tic)

    tic = time.perf_counter()
    data = []
    for idx, row in prices.iterrows():
        data_row = CryptoData(0, float(row.price), float(row.marketcap), float(row.total_volume), idx.strftime('%Y-%m-%d'), symbol)
        data.append(data_row.__dict__)
    toc = time.perf_counter()
    res = requests.post("http://localhost:8080/market_chart/insertMultipleCMC", json = data)

    print('REQ', toc-tic)
    
    

def main():
       
    cnx = mysql.connector.connect(user='root', password='root',
                              host='localhost', database='r2g_crypto',
                              port=3308)
    cursor = cnx.cursor()
    query = ('Select  * from currencies')
    cursor.execute(query)
    tic = time.perf_counter()
    for (id, name) in cursor:
        syncInsert(id, name.lower())
    
    toc = time.perf_counter()
    print(toc-tic)

if __name__ ==  '__main__':
    main()


