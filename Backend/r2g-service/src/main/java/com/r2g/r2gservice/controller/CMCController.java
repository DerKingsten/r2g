package com.r2g.r2gservice.controller;

import java.util.List;

import com.r2g.r2gservice.model.CurrencyMarketChart;
import com.r2g.r2gservice.services.CMCService;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/market_chart")
public class CMCController{

    private Logger logger = LoggerFactory.getLogger(CMCController.class);

    @Autowired
    private CMCService cmcService; 

    @PostMapping("/insertMultipleCMC")
    public void insertMultipleCMC(@RequestBody List<CurrencyMarketChart> cmcList){
        cmcService.insertMarketChart(cmcList);
    }

    @PostMapping("/insertSingleCMC")
    public void insertSingleCMC(@RequestBody CurrencyMarketChart cmc){
        cmcService.insertMarketChart(cmc);
    }

    @GetMapping("/getMarketChart")
    public List<CurrencyMarketChart> getMarketChart(@RequestParam String currency_id){
        return cmcService.findByCurrencyId(currency_id);
    }
}