package com.r2g.r2gservice.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity
@Table(name = "currency_market_chart")
public class CurrencyMarketChart {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private int id;

  private Double price;

  private Double marketcap;

  @Column(name = "total_volume")
  private Double totalVolume;

  private Date date;

  @Column(name = "currency_id")
  private String currencyId;

  public CurrencyMarketChart() {

  }

  public Double getTotalVolume() {
    return totalVolume;
  }

  public void setTotalVolume(Double totalVolume) {
    this.totalVolume = totalVolume;
  }

  public Double getPrice() {
    return price;
  }

  public void setPrice(Double price) {
    this.price = price;
  }

  public String getCurrencyId() {
    return currencyId;
  }

  public void setCurrencyId(String currencyId) {
    this.currencyId = currencyId;
  }

  public Double getMarketcap() {
    return marketcap;
  }

  public void setMarketcap(Double marketcap) {
    this.marketcap = marketcap;
  }

  public Date getDate() {
    return date;
  }

  public void setDate(Date date) {
    this.date = date;
  }

  public CurrencyMarketChart(Double value) {
    this.setPrice(value);
  }

  public CurrencyMarketChart(Double price, Double marketcap, Double totalVolume, Date date, String currencyId) {
    this.price = price;
    this.marketcap = marketcap;
    this.setTotalVolume(totalVolume);
    this.date = date;
    this.currencyId = currencyId;
    
  }

 
}
