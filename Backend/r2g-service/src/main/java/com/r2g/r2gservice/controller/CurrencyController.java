package com.r2g.r2gservice.controller;

import java.util.List;

import com.r2g.r2gservice.model.Currency;
import com.r2g.r2gservice.services.CurrencyService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/currencies")
public class CurrencyController{

    @Autowired
    CurrencyService service;

    @GetMapping("/getAllCurrencies")
    public List<Currency> getAllCurrencies() {
        return service.getAllCurrencies();
    }

    @GetMapping("/getCurrencyById")
    public Currency getCurrencyById(@RequestParam String id) {
        return service.getCurrencyById(id);
    }
    

    
    
}
