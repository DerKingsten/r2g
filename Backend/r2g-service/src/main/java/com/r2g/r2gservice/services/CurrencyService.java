package com.r2g.r2gservice.services;

import java.util.List;
import java.util.Optional;

import com.r2g.r2gservice.model.Currency;
import com.r2g.r2gservice.repositories.CurrencyRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CurrencyService{

    @Autowired
    CurrencyRepository repository;

    public List<Currency> getAllCurrencies() {
            return (List<Currency>) repository.findAll();
    }

    public Currency getCurrencyById(String id) {
        Optional<Currency> currencyById =  repository.findById(id);
        if(currencyById.isPresent()){
            return currencyById.get();
        } else {
            return null;
        }
    }
    
    // public long getCurrencyCount(){
    //     return 100;
    // }
}
