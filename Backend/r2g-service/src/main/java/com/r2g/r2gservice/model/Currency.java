package com.r2g.r2gservice.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "currencies")
public class Currency {

    @Id
    private String id;
    private String name;

    public Currency(String id, String name) {
        this.id = id;
        this.name = name;
    }

    public Currency() {
    }

    public String getName() {
        return name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

}
