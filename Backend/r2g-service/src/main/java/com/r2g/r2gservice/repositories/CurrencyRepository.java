package com.r2g.r2gservice.repositories;

import com.r2g.r2gservice.model.Currency;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CurrencyRepository extends CrudRepository<Currency, String>{
    // long countById();
    
}
