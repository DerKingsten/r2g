package com.r2g.r2gservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@SpringBootApplication
public class R2gServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(R2gServiceApplication.class, args);
	}

	private static final String[] HOST = { "http://localhost:5500" };

	@Bean
	public WebMvcConfigurer corsConfigurer() {
		return new WebMvcConfigurer() {
			@Override
			public void addCorsMappings(CorsRegistry registry) {
				registry.addMapping("/*").allowedOrigins(HOST);
				registry.addMapping("/currencies/*").allowedOrigins(HOST);
				registry.addMapping("/marketcap/*").allowedOrigins(HOST);
				registry.addMapping("/market_chart/*").allowedOrigins(HOST);
			}
		};
	}
}
