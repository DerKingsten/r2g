package com.r2g.r2gservice.repositories;

import org.springframework.stereotype.Repository;

import java.util.List;

import com.r2g.r2gservice.model.CurrencyMarketChart;

import org.springframework.data.repository.CrudRepository;

@Repository
public interface CMCRepository extends CrudRepository<CurrencyMarketChart, Integer>{
    List<CurrencyMarketChart> findTop365ByCurrencyIdOrderByDateAsc(String currency_id);
    

}
