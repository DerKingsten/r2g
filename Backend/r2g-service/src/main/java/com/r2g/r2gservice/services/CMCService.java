package com.r2g.r2gservice.services;

import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Optional;


import com.r2g.r2gservice.model.CurrencyMarketChart;
import com.r2g.r2gservice.repositories.CMCRepository;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CMCService {
    
    @Autowired
    CMCRepository cmcRepository;

    Logger logger = LoggerFactory.getLogger(CMCService.class);

    Calendar calendar = Calendar.getInstance();

    public List<CurrencyMarketChart> findAll(){
        return (List<CurrencyMarketChart>) cmcRepository.findAll();
    }

    public List<CurrencyMarketChart> findByCurrencyId(String currency_id){
        return cmcRepository.findTop365ByCurrencyIdOrderByDateAsc(currency_id);
    }

    public void insertMarketChart(CurrencyMarketChart cmc){
        cmcRepository.save(cmc);
    }

    public void insertMarketChart(List<CurrencyMarketChart> cmcList){
        cmcRepository.saveAll(cmcList);
    }

}
